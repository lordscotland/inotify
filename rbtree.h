struct rb_node {
  struct rb_node* P;
  struct rb_node* L;
  struct rb_node* R;
  int red;
};
typedef int(*rb_node_compare_func)(const struct rb_node*,const struct rb_node*);
typedef void(*rb_node_destroy_func)(struct rb_node*);
typedef int(*rb_node_each_func)(struct rb_node*,void*);
struct rb_tree {
  struct rb_node* root;
  rb_node_compare_func compare;
  rb_node_destroy_func destroy;
};
struct rb_tree* rb_new(rb_node_compare_func compare,rb_node_destroy_func destroy);
int rb_foreach(struct rb_tree* tree,rb_node_each_func each,void* context);
struct rb_node* rb_get(struct rb_tree* tree,const struct rb_node* key,size_t newsize);
void rb_delete(struct rb_tree* tree,struct rb_node* node);
void rb_destroy(struct rb_tree* tree);
