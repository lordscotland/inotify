#include <errno.h>
#include <fcntl.h>
#include <fts.h>
#include <pthread.h>
#include <time.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/inotify.h>
#include <sys/ioctl.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <unistd.h>
#include "rbtree.h"

typedef struct {
  unsigned long rc;
  struct dirref* dirref;
  char* name;
  char value[];
} Path;
struct dirref {
  unsigned long rc;
  struct dir_node* dir;
};
struct dir_node {
  struct rb_node _;
  struct dir_node* parent;
  struct rb_tree* children;
  struct dirref* dirref;
  char* name;
  int wd,moved;
};
struct child_node {
  struct rb_node _;
  struct dir_node* dir;
};
struct file_node {
  struct rb_node _;
  Path* path;
  int create;
};
struct QueueItem {
  struct QueueItem* next;
};
struct Action {
  struct QueueItem _;
  Path* path1;
  Path* path2;
  uint32_t mask;
  unsigned int index;
};
struct Event {
  struct QueueItem _;
  Path* path;
  uint32_t mask;
  uint32_t cookie;
};
struct Queue {
  struct QueueItem* first;
  struct QueueItem* last;
  pthread_cond_t cond;
  pthread_mutex_t lock;
};
struct ActionQueue {
  struct Queue _;
  char* arg;
  int* counter;
  int cwd;
};
struct EventQueue {
  struct Queue _;
  struct ActionQueue* aqlist;
  int aqcount;
  unsigned int index;
};

#define NSEC_PER_SEC 1000000000
static void timespec_add(struct timespec* tspec,time_t sec,long nsec){
  if(nsec){
    const time_t add=nsec/NSEC_PER_SEC;
    if(add){sec+=add;nsec-=add*NSEC_PER_SEC;}
    if(tspec->tv_nsec<-nsec){sec--;nsec+=NSEC_PER_SEC;}
    else if(tspec->tv_nsec>=NSEC_PER_SEC-nsec){sec++;nsec-=NSEC_PER_SEC;}
  }
  tspec->tv_sec+=sec;
  tspec->tv_nsec+=nsec;
}
static char* string_copy(const char* string){
  const size_t size=strlen(string)+1;
  return memcpy(malloc(size),string,size);
}
static struct dirref* dirref_retain(struct dirref* dirref){
  if(dirref){__sync_add_and_fetch(&dirref->rc,1);}
  return dirref;
}
static void dirref_release(struct dirref* dirref){
  if(dirref && !__sync_sub_and_fetch(&dirref->rc,1)){free(dirref);}
}
static Path* path_retain(Path* path){
  if(path){__sync_add_and_fetch(&path->rc,1);}
  return path;
}
static void path_release(Path* path){
  if(path && !__sync_sub_and_fetch(&path->rc,1)){
    dirref_release(path->dirref);
    free(path);
  }
}
static int path_compare(Path* pathA,Path* pathB){
  int dircmp=pathA->dirref-pathB->dirref;
  return dircmp?dircmp:strcmp(pathA->name,pathB->name);
}
static Path* dir_copy_path(struct dir_node* dir,const char* name){
  size_t size=0;
  struct dir_node* pdir;
  for (struct dir_node* cdir=dir;(pdir=cdir->parent);cdir=pdir){
    size+=strlen(cdir->name)+1;
  }
  if(!size && !name){return NULL;}
  const size_t namesize=name?strlen(name)+1:0;
  Path* path=malloc(sizeof(*path)+size+namesize);
  path->rc=1;
  path->dirref=name?dirref_retain(dir->dirref):NULL;
  char* ptr=path->value+size;
  path->name=name?memcpy(ptr,name,namesize):NULL;
  for (struct dir_node* cdir=dir;(pdir=cdir->parent);cdir=pdir){
    const size_t namelen=strlen(cdir->name);
    *(--ptr)=(cdir==dir && !name)?0:'/';
    memcpy(ptr-=namelen,cdir->name,namelen);
  }
  return path;
}
static void queue_init(struct Queue* queue){
  queue->first=NULL;
  pthread_cond_init(&queue->cond,NULL);
  pthread_mutex_init(&queue->lock,NULL);
}
static void queue_destroy(struct Queue* queue){
  pthread_cond_destroy(&queue->cond);
  pthread_mutex_destroy(&queue->lock);
}
static void queue_append(struct Queue* queue,struct QueueItem* first,struct QueueItem* last){
  pthread_mutex_lock(&queue->lock);
  *(queue->first?&queue->last->next:&queue->first)=first;
  queue->last=last?last:first;
  pthread_cond_signal(&queue->cond);
  pthread_mutex_unlock(&queue->lock);
}
static struct QueueItem* queue_read(struct Queue* queue){
  struct QueueItem* item;
  pthread_mutex_lock(&queue->lock);
  while(!(item=queue->first)){
    pthread_cond_wait(&queue->cond,&queue->lock);
  }
  queue->first=NULL;
  pthread_mutex_unlock(&queue->lock);
  return item;
}
static struct QueueItem* queue_scan(struct Queue* queue,struct QueueItem* last,int(*predicate)(const struct QueueItem*,void*),void* context,struct timespec* tspec){
  int status=tspec?0:-1;
  do {
    struct QueueItem* item;
    while((item=last->next)){
      int result=predicate(item,context);
      if(result){
        if(result>0){last->next=item->next;}
        return item;
      }
      last=item;
    }
    pthread_mutex_lock(&queue->lock);
    while(!(last->next=queue->first) && status==0){
      status=pthread_cond_timedwait(&queue->cond,&queue->lock,tspec);
    }
    queue->first=NULL;
    pthread_mutex_unlock(&queue->lock);
  } while(status==0);
  return NULL;
}
static void enqueue_action(struct EventQueue* equeue,uint32_t mask,Path* path1,Path* path2){
  const unsigned int index=mask?++equeue->index:0;
  if(mask){
    const char* slash=(mask&IN_ISDIR)?"/":"";
    printf(path2?"<%u> %s %s%s => %s%s\n":"<%u> %s %s%s\n",index,
     (mask&IN_MOVE)?"MOVE":(mask&IN_CREATE)?"CREATE":(mask&IN_MODIFY)?"MODIFY":
     (mask&IN_DELETE)?"DELETE":(mask&IN_ATTRIB)?"ATTRIB":"???",
     path1->value,slash,path2->value,slash);
  }
  const int aqcount=equeue->aqcount;
  for (int i=0;i<aqcount;i++){
    struct Action* action=malloc(sizeof(*action));
    action->path1=path_retain(path1);
    action->path2=path_retain(path2);
    action->mask=mask;
    action->index=index;
    struct QueueItem* item=(void*)action;
    item->next=NULL;
    queue_append((void*)&equeue->aqlist[i],item,NULL);
  }
}
static void action_destroy(struct Action* action){
  path_release(action->path1);
  path_release(action->path2);
  free(action);
}
struct action_match_path {
  Path* path;
  uint32_t mask;
  int status;
};
static int action_match_path(const struct QueueItem* item,void* context){
  const struct Action* action=(void*)item;
  struct action_match_path* ref=context;
  if(!(action->mask&(IN_ISDIR|IN_IGNORED))){
    if(action->mask&(IN_MOVE|IN_DELETE)){
      int cmp=path_compare(ref->path,action->path1);
      if(action->mask&IN_MOVE){
        if(cmp==0){
          ref->path=action->path2;
          return ref->status=-1;
        }
        cmp=path_compare(ref->path,action->path2);
      }
      if(cmp==0){
        ref->path=NULL;
        return ref->status=-1;
      }
    }
    else if(action->mask&ref->mask && path_compare(ref->path,action->path1)==0){return -1;}
  }
  return 0;
}
static int action_file_open(struct ActionQueue* aqueue,struct Action* action){
  struct action_match_path V_ref;
  V_ref.path=action->path1;
  V_ref.mask=action->mask;
  int fd,errnum;
  do {
    fd=open(V_ref.path->value,O_RDONLY|O_CLOEXEC);
    errnum=(fd<0)?errno:0;
    struct timespec timeout;
    clock_gettime(CLOCK_REALTIME,&timeout);
    timespec_add(&timeout,0,NSEC_PER_SEC/8);
    V_ref.status=0;
    while((action=(void*)queue_scan((void*)aqueue,(void*)action,action_match_path,&V_ref,&timeout))){
      if(V_ref.status){
        if(fd>=0){close(fd);}
        if(V_ref.path){break;}
        errno=0;
        return -1;
      }
      action->mask=IN_IGNORED;
    }
  } while(action);
  errno=errnum;
  return fd;
}
static void* process_actions(void* context){
  struct ActionQueue* aqueue=context;
  char root[]="/tmp/rclone-monitor.XXXXXX";
  if(!mkdtemp(root)){perror("E_mkdtemp");return NULL;}
  const pid_t pid=fork();
  if(pid<0){perror("E_fork");return NULL;}
  if(pid==0){
    setsid();
    fchdir(aqueue->cwd);
    exit(execlp("rclone","rclone","mount",aqueue->arg,root,NULL));
  }
  if(!__sync_sub_and_fetch(aqueue->counter,1)){close(aqueue->cwd);}
  int rootfd;
  while(1){
    usleep(100000);
    rootfd=open(root,O_RDONLY|O_DIRECTORY|O_CLOEXEC);
    if(rootfd<0){continue;}
    struct stat stbuf;
    fstat(rootfd,&stbuf);
    if(stbuf.st_nlink==1){break;}
    close(rootfd);
  }
  printf("[%s] > %s\n",aqueue->arg,root);
  int run=1;
  do {
    struct QueueItem* item=queue_read(context);
    do {
      struct Action* action=(void*)item;
      const char* label=NULL;
      int errnum;
      if(!action->mask){run=-1;}
      else if(action->mask&IN_MOVE && action->path2){
        label="mv";errnum=renameat(rootfd,action->path1->value,
         rootfd,action->path2->value)?errno:0;
      }
      else if(action->mask&IN_ISDIR){
        if(action->mask&IN_CREATE){
          label="mkdir";errnum=mkdirat(rootfd,action->path1->value,0777)?errno:0;
        }
        else if(action->mask&IN_DELETE){
          label="rmdir";errnum=unlinkat(rootfd,action->path1->value,AT_REMOVEDIR)?errno:0;
        }
      }
      else if(action->mask&(IN_CREATE|IN_MODIFY)){
        action->mask=IN_CREATE|IN_MODIFY|IN_ATTRIB;
        int fd=action_file_open(aqueue,action);
        if(fd<0){
          if(errno){label="cp.open(r)";errnum=errno;}
        }
        else {
          struct stat stbuf;
          if(fstat(fd,&stbuf)<0){label="cp.fstat";errnum=errno;}
          else if(S_ISREG(stbuf.st_mode)){
            int ofd=openat(rootfd,action->path1->value,O_WRONLY|O_CREAT|O_TRUNC,0666);
            if(ofd<0){label="cp.open(w)";errnum=errno;}
            else {
              label="cp";errnum=0;
              char buf[4096];
              ssize_t nread;
              while((nread=read(fd,buf,sizeof(buf)))>0){
                char* ptr=buf;
                ssize_t nwrite;
                while((nwrite=write(ofd,ptr,nread))>=0 && (nread-=nwrite)){ptr+=nwrite;}
                if(nwrite<0 || nread){nread=-1;break;}
              }
              futimens(ofd,(struct timespec[]){stbuf.st_atim,stbuf.st_mtim});
              if(nread<0){label="cp.read";errnum=errno;}
              if(close(ofd)<0){label="cp.write";errnum=errno;}
            }
          }
          close(fd);
        }
      }
      else if(action->mask&IN_DELETE){
        label="rm";errnum=unlinkat(rootfd,action->path1->value,0)?errno:0;
      }
      else if(action->mask&IN_ATTRIB){
        int fd=action_file_open(aqueue,action);
        if(fd<0){
          if(errno){label="touch.open(r)";errnum=errno;}
        }
        else {
          struct stat stbuf;
          if(fstat(fd,&stbuf)<0){label="touch.fstat";errnum=errno;}
          else {
            label="touch";errnum=(utimensat(rootfd,action->path1->value,
             (struct timespec[]){stbuf.st_atim,stbuf.st_mtim},AT_SYMLINK_NOFOLLOW)<0)?errno:0;
          }
          close(fd);
        }
      }
      if(label){
        printf("[%s] <%u> %s: %s\n",aqueue->arg,action->index,
         label,errnum?strerror(errnum):"OK");
      }
      else if(run<0){run=0;}
      else {printf("[%s] <%u> (skip)\n",aqueue->arg,action->index);}
      item=item->next;
      action_destroy(action);
    } while(item);
  } while(run);
  close(rootfd);
  kill(pid,SIGTERM);
  waitpid(pid,NULL,0);
  if(rmdir(root)){printf("[%s] rmdir '%s': %s",aqueue->arg,root,strerror(errno));}
  return NULL;
}
static int file_compare(const struct rb_node* A,const struct rb_node* B){
  return path_compare(((const struct file_node*)A)->path,
   ((const struct file_node*)B)->path);
}
static void file_destroy(struct rb_node* node){
  path_release(((struct file_node*)node)->path);
}
static int file_commit(struct rb_node* node,void* context){
  struct file_node* file=(void*)node;
  struct dir_node* dir=file->path->dirref->dir;
  if(dir){
    Path* path=dir_copy_path(dir,file->path->name);
    enqueue_action(context,file->create?IN_CREATE:IN_MODIFY,path,NULL);
    path_release(path);
  }
  return 1;
}
static struct file_node* file_get(struct rb_tree* files,Path* path,int create){
  struct file_node V_file;V_file.path=path;
  struct file_node* file=(void*)rb_get(files,(void*)&V_file,create?sizeof(*file):0);
  if(file && !file->path){file->path=path_retain(path);}
  return file;
}
static struct QueueItem* Event(Path* path,uint32_t mask,uint32_t cookie){
  struct Event* event=malloc(sizeof(*event));
  event->path=path_retain(path);
  event->mask=mask;
  event->cookie=cookie;
  struct QueueItem* item=(void*)event;
  item->next=NULL;
  return item;
}
static void event_destroy(struct Event* event){
  path_release(event->path);
  free(event);
}
static int event_match_movedto(const struct QueueItem* item,void* context){
  const struct Event* event=(void*)item;
  const struct Event* ref=(void*)context;
  return event->mask&IN_MOVED_TO && event->cookie==ref->cookie;
}
static void* process_events(void* context){
  struct EventQueue* equeue=context;
  struct rb_tree* files=rb_new(file_compare,file_destroy);
  int run=1;
  do {
    struct QueueItem* item=queue_read(context);
    do {
      struct Event* event=(void*)item;
      if(!event->mask){run=0;}
      else if(event->mask&IN_ISDIR){
        if(event->mask&IN_MOVED_FROM){
          struct Event* event0=event;
          event=(void*)(item=item->next);
          enqueue_action(context,IN_MOVE|IN_ISDIR,event0->path,event->path);
          event_destroy(event0);
        }
        else {enqueue_action(context,event->mask,event->path,NULL);}
      }
      else if(event->mask&IN_MOVED_FROM){
        struct timespec timeout;
        clock_gettime(CLOCK_REALTIME,&timeout);
        timespec_add(&timeout,0,NSEC_PER_SEC/10);
        struct Event* match=(void*)queue_scan(context,item,event_match_movedto,event,&timeout);
        if(!match){goto __DELETE;}
        struct file_node* file=file_get(files,event->path,0);
        if(!file || !file->create){enqueue_action(context,IN_MOVE,event->path,match->path);}
        if(file){
          struct file_node* newfile=file_get(files,match->path,1);
          if(newfile!=file){
            newfile->create=file->create;
            rb_delete(files,(void*)file);
          }
        }
        event_destroy(match);
      }
      else if(event->mask&IN_MOVED_TO){
        enqueue_action(context,IN_CREATE,event->path,NULL);
      }
      else if(event->mask&IN_CREATE){
        file_get(files,event->path,1)->create=1;
      }
      else if(event->mask&IN_MODIFY){
        file_get(files,event->path,1);
      }
      else if(event->mask&IN_CLOSE_WRITE){
        struct file_node* file=file_get(files,event->path,0);
        if(file){
          enqueue_action(context,file->create?IN_CREATE:IN_MODIFY,event->path,NULL);
          rb_delete(files,(void*)file);
        }
      }
      else if(event->mask&IN_ATTRIB){
        if(!file_get(files,event->path,0)){
          enqueue_action(context,IN_ATTRIB,event->path,NULL);
        }
      }
      else if(event->mask&IN_DELETE) __DELETE:{
        struct file_node* file=file_get(files,event->path,0);
        if(!file || !file->create){enqueue_action(context,IN_DELETE,event->path,NULL);}
        if(file){rb_delete(files,(void*)file);}
      }
      item=item->next;
      event_destroy(event);
    } while(item);
  } while(run);
  rb_foreach(files,file_commit,equeue);
  rb_destroy(files);
  return NULL;
}
static int child_compare(const struct rb_node* A,const struct rb_node* B){
  return strcmp(((struct child_node*)A)->dir->name,((struct child_node*)B)->dir->name);
}
static int child_commit_r(struct rb_node* node,void* context){
  struct dir_node* dir=((struct child_node*)node)->dir;
  rb_foreach(dir->children,child_commit_r,context);
  dir->dirref->dir=NULL;
  dir->moved=0;
  Path* path=dir_copy_path(dir,NULL);
  queue_append(context,Event(path,IN_DELETE|IN_ISDIR,0),NULL);
  path_release(path);
  return 1;
}
static int child_delete_r(struct rb_node* node,void* context){
  struct dir_node* dir=((struct child_node*)node)->dir;
  rb_foreach(dir->children,child_delete_r,context);
  dir->dirref->dir=NULL;
  rb_delete(context,(void*)dir);
  return 1;
}
static int dir_compare(const struct rb_node* A,const struct rb_node* B){
  return ((const struct dir_node*)A)->wd-((const struct dir_node*)B)->wd;
}
static void dir_destroy(struct rb_node* node){
  struct dir_node* dir=(void*)node;
  rb_destroy(dir->children);
  dirref_release(dir->dirref);
  free(dir->name);
}
static int dir_commit(struct rb_node* node,void* context){
  struct dir_node* dir=(void*)node;
  if(dir->moved){
    struct child_node V_child;V_child.dir=dir;
    child_commit_r((void*)&V_child,context);
  }
  return 1;
}
static struct QueueItem* dir_move(struct dir_node* dir,Path* path,struct QueueItem** oLast){
  struct QueueItem* item;
  Path* path0=dir_copy_path(dir,NULL);
  if(strcmp(path0->value,path->value)){
    item=Event(path0,IN_MOVED_FROM|IN_ISDIR,0);
    *oLast=item->next=Event(path,IN_MOVED_TO|IN_ISDIR,0);
  }
  else {item=*oLast=NULL;}
  path_release(path0);
  return item;
}
static void dir_assign(struct dir_node* dir,struct dir_node* parent,const char* name){
  if(parent){
    free(dir->name);
    dir->name=name?string_copy(name):NULL;
  }
  if(dir->parent!=parent){
    struct child_node V_child;V_child.dir=dir;
    if(dir->parent){
      struct rb_tree* children=dir->parent->children;
      struct rb_node* node=(void*)rb_get(children,(void*)&V_child,0);
      if(node){rb_delete(children,node);}
    }
    if((dir->parent=parent)){
      struct child_node* child=(void*)rb_get(parent->children,(void*)&V_child,sizeof(*child));
      child->dir=dir;
    }
  }
}
static int watch(int infd,struct rb_tree* dirs,const char* accpath,struct dir_node** oDir){
  int wd=inotify_add_watch(infd,accpath,IN_CREATE|IN_MODIFY|IN_CLOSE_WRITE
   |IN_ATTRIB|IN_MOVE|IN_DELETE|IN_EXCL_UNLINK|IN_ONLYDIR);
  if(wd<0){return -1;}
  struct dir_node V_dir;V_dir.wd=wd;
  struct dir_node* dir=*oDir=(void*)rb_get(dirs,(void*)&V_dir,sizeof(*dir));
  if(dir->children){return dir->moved=0;}
  dir->children=rb_new(child_compare,NULL);
  struct dirref* dirref=dir->dirref=malloc(sizeof(*dirref));
  dirref->rc=1;
  dirref->dir=dir;
  dir->wd=wd;
  return 1;
}
static void watch_r(int infd,struct rb_tree* dirs,struct dir_node* parent,const char* accpath,struct Queue* queue){
  struct QueueItem* first=NULL;
  struct QueueItem* last;
  FTS* ftsp=fts_open((char*[]){(char*)accpath,NULL},FTS_PHYSICAL|FTS_NOSTAT,NULL);
  FTSENT* ent;
  while((ent=fts_read(ftsp))){
    int move=0;
    struct dir_node* cdir;
    if(ent->fts_info==FTS_D){
      int status=watch(infd,dirs,ent->fts_accpath,&cdir);
      if(status>0){ent->fts_pointer=cdir;}
      else {
        fts_set(ftsp,ent,FTS_SKIP);
        if(status){continue;}
        move=1;
      }
    }
    else if(queue && ent->fts_info==FTS_F){cdir=NULL;}
    else {continue;}
    struct dir_node* pdir=(ent->fts_level>0)?ent->fts_parent->fts_pointer:parent;
    if(queue){
      const size_t size=ent->fts_pathlen+1;
      Path* path=malloc(sizeof(*path)+size);
      path->rc=1;
      path->dirref=cdir?NULL:dirref_retain(pdir->dirref);
      path->name=cdir?NULL:path->value+size-1-ent->fts_namelen;
      memcpy(&path->value,ent->fts_path,size);
      struct QueueItem** oItem=first?&last->next:&first;
      *oItem=cdir?move?dir_move(cdir,path,&last):
       (last=Event(path,IN_CREATE|IN_ISDIR,0)):
       (last=Event(path,IN_CREATE,0));
      path_release(path);
    }
    if(cdir){dir_assign(cdir,pdir,ent->fts_name);}
  }
  fts_close(ftsp);
  if(first){queue_append(queue,first,last);}
}
static void handle_signal(int signal) {
  printf("(Received signal %d)\n",signal);
}

int main(int argc,char** argv){
  if(argc<2){
    fprintf(stderr,"Usage: %s <local> [<remote> ...]\n",argv[0]);
    return 1;
  }
  int infd=inotify_init();
  if(infd<0){perror("E_inotify");return -1;}
  int cwd=open(".",O_RDONLY|O_DIRECTORY|O_CLOEXEC);
  if(cwd<0){perror("E_open(.)");return -1;}
  if(chdir(argv[1])){perror("E_chdir");return -1;}
  struct EventQueue V_equeue;
  const int aqcount=V_equeue.aqcount=argc-2;
  V_equeue.aqlist=malloc(aqcount*sizeof(*V_equeue.aqlist));
  V_equeue.index=0;
  pthread_t* athreads=aqcount?malloc(aqcount*sizeof(*athreads)):NULL;
  int count=aqcount;
  for (int i=0;i<aqcount;i++){
    struct ActionQueue* aqueue=&V_equeue.aqlist[i];
    aqueue->arg=argv[2+i];
    aqueue->counter=&count;
    aqueue->cwd=cwd;
    queue_init((void*)aqueue);
    if(pthread_create(&athreads[i],NULL,process_actions,aqueue)){
      perror("E_pthread_create(athread)");return -1;
    }
  }
  struct Queue* equeue=(void*)&V_equeue;
  queue_init(equeue);
  pthread_t ethread;
  if(pthread_create(&ethread,NULL,process_events,equeue)){
    perror("E_pthread_create(ethread)");return -1;
  }
  struct sigaction action;
  action.sa_handler=handle_signal;
  action.sa_flags=SA_RESETHAND;
  sigemptyset(&action.sa_mask);
  if(sigaction(SIGINT,&action,NULL)){perror("W_sigaction");}
  struct rb_tree* dirs=rb_new(dir_compare,dir_destroy);
  watch_r(infd,dirs,NULL,".",NULL);
  size_t ebufsize=4096;
  struct inotify_event* ebuf=malloc(ebufsize);
  puts("Monitoring local directory...");
  ssize_t rlen;
  while((rlen=read(infd,ebuf,ebufsize))){
    if(rlen<0){
      if(errno!=EINVAL){break;}
      if(ioctl(infd,FIONREAD,&ebufsize)){perror("E_ioctl");break;}
      free(ebuf);
      ebuf=malloc(ebufsize+=4096);
      rlen=read(infd,ebuf,ebufsize);
      if(rlen<=0){break;}
    }
    struct inotify_event* eptr=ebuf;
    struct inotify_event* eptrx=(void*)((char*)eptr+rlen);
    for (;eptr<eptrx;eptr=(void*)((char*)eptr+sizeof(*eptr)+eptr->len)){
      struct dir_node V_dir;V_dir.wd=eptr->wd;
      struct dir_node* dir=(void*)rb_get(dirs,(void*)&V_dir,0);
      char* name=eptr->len?eptr->name:NULL;
      if(!dir){
        fprintf(stderr,"W_read: wd=%d mask=%x cookie=%x name=%s\n",
         eptr->wd,eptr->mask,eptr->cookie,name);
        continue;
      }
      if(eptr->mask&IN_IGNORED){
        dir_commit((void*)dir,equeue);
        dir_assign(dir,NULL,NULL);
        struct child_node V_child;V_child.dir=dir;
        child_delete_r((void*)&V_child,dirs);
        continue;
      }
      Path* path=dir_copy_path(dir,name);
      if(eptr->mask&IN_ISDIR){
        if(eptr->mask&IN_CREATE){
          struct dir_node* cdir;
          int status=watch(infd,dirs,path->value,&cdir);
          if(status>=0){
            if(status){queue_append(equeue,Event(path,IN_CREATE|IN_ISDIR,0),NULL);}
            else {
              struct QueueItem* last;
              struct QueueItem* first=dir_move(cdir,path,&last);
              queue_append(equeue,first,last);
            }
            dir_assign(cdir,dir,name);
          }
        }
        else if(eptr->mask&IN_DELETE){
          queue_append(equeue,Event(path,IN_DELETE|IN_ISDIR,0),NULL);
        }
        else if(eptr->mask&IN_MOVED_FROM){
          struct dir_node V_dir;V_dir.name=name;
          struct child_node V_child;V_child.dir=&V_dir;
          struct child_node* child=(void*)rb_get(dir->children,(void*)&V_child,0);
          if(child){child->dir->moved=1;}
        }
        else if(eptr->mask&IN_MOVED_TO){watch_r(infd,dirs,dir,path->value,equeue);}
      }
      else {queue_append(equeue,Event(path,eptr->mask,eptr->cookie),NULL);}
      path_release(path);
    }
  }
  puts("Finalizing...");
  close(infd);
  free(ebuf);
  rb_foreach(dirs,dir_commit,equeue);
  queue_append(equeue,Event(NULL,0,0),NULL);
  pthread_join(ethread,NULL);
  enqueue_action((void*)equeue,0,NULL,NULL);
  for (int i=0;i<aqcount;i++){
    pthread_join(athreads[i],NULL);
    queue_destroy((void*)&V_equeue.aqlist[i]);
  }
  rb_destroy(dirs);
  free(athreads);
  free(V_equeue.aqlist);
  queue_destroy(equeue);
}
