#include <stdlib.h>
#include "rbtree.h"

static void _rb_transplant(struct rb_node* x,struct rb_node* y){
  struct rb_node* P=x->P;
  if(P){*(x==P->L?&P->L:&P->R)=y;}
  if(y){y->P=P;}
}
static struct rb_node* _rb_rotate(struct rb_node* x,int rL){
  struct rb_node** xcp=rL?&x->R:&x->L;
  struct rb_node* y=*xcp;
  struct rb_node** ycp=rL?&y->L:&y->R;
  if((*xcp=*ycp)){(*xcp)->P=x;}
  _rb_transplant(x,y);
  (*ycp=x)->P=y;
  return y;
}
struct rb_tree* rb_new(rb_node_compare_func compare,rb_node_destroy_func destroy) {
  struct rb_tree* tree=malloc(sizeof(*tree));
  tree->root=NULL;
  tree->compare=compare;
  tree->destroy=destroy;
  return tree;
}
int _rb_foreach(struct rb_node* node,rb_node_each_func each,void* context){
  return !node || (_rb_foreach(node->L,each,context) && each(node,context)
   && _rb_foreach(node->R,each,context));
}
int rb_foreach(struct rb_tree* tree,rb_node_each_func each,void* context){
  return _rb_foreach(tree->root,each,context);
}
struct rb_node* rb_get(struct rb_tree* tree,const struct rb_node* key,size_t newsize){
  struct rb_node** nodep=&tree->root;
  struct rb_node* P=NULL;
  struct rb_node* node;
  while((node=*nodep)){
    int cmp=tree->compare(key,node);
    if(cmp==0){break;}
    P=node;
    nodep=(cmp<0)?&P->L:&P->R;
  }
  if(!node && newsize){
    node=*nodep=calloc(1,newsize);
    if((node->P=P)){node->red=1;}
    else {(tree->root=node)->red=0;}
    struct rb_node* x=node;
    while((P=x->P) && P->red){
      int isL=(P==P->P->L);
      struct rb_node* PPc=isL?P->P->R:P->P->L;
      if(PPc && PPc->red){
        P->red=PPc->red=0;
        if((x=P->P)->P){x->red=1;}
      }
      else {
        if(x==(isL?P->R:P->L)){P=_rb_rotate(x=P,isL);}
        P->red=0;
        P->P->red=1;
        P=_rb_rotate(P->P,!isL);
        if(!P->P){(tree->root=P)->red=0;}
      }
    }
  }
  return node;
}
void rb_delete(struct rb_tree* tree,struct rb_node* node){
  struct rb_node* x=node->R;
  struct rb_node* P;
  if(x && node->L){
    struct rb_node* y=x;
    while(y->L){y=y->L;}
    int deep=(y!=x);
    x=y->R;
    if(deep){
      _rb_transplant(y,x);
      (y->R=node->R)->P=y;
    }
    _rb_transplant(node,y);
    (y->L=node->L)->P=y;
    if(!node->P){tree->root=y;}
    P=y->red?NULL:deep?y->P:y;
    y->red=node->red;
  }
  else {
    if(!x){x=node->L;}
    _rb_transplant(node,x);
    P=node->red?NULL:node->P;
    if(!node->P && (tree->root=x)){x->red=0;}
  }
  rb_node_destroy_func destroy=tree->destroy;
  if(destroy){destroy(node);}
  free(node);
  while(P){
    if(x && x->red){
      x->red=0;
      break;
    }
    int isL=(x==P->L);
    struct rb_node* w=isL?P->R:P->L;
    if(w && w->red){
      _rb_rotate(P,isL);
      if(!w->P){tree->root=w;}
      w->red=0;
      P->red=1;
      w=isL?P->R:P->L;
    }
    if(w){
      struct rb_node* wc1r=isL?w->R:w->L;
      struct rb_node* wc2r=isL?w->L:w->R;
      if(wc1r && !wc1r->red){wc1r=NULL;}
      if(wc2r && !wc2r->red){wc2r=NULL;}
      if(wc1r || wc2r){
        if(wc1r){wc1r->red=0;}
        else {
          if(wc2r){wc2r->red=0;}
          w=_rb_rotate(w,!isL);
        }
        _rb_rotate(P,isL);
        if(!w->P){(tree->root=w)->red=0;}
        else {w->red=P->red;}
        P->red=0;
        break;
      }
      w->red=1;
    }
    P=(x=P)->P;
  }
}
static void _rb_destroy(rb_node_destroy_func destroy,struct rb_node* node){
  if(!node){return;}
  _rb_destroy(destroy,node->L);
  _rb_destroy(destroy,node->R);
  if(destroy){destroy(node);}
  free(node);
}
void rb_destroy(struct rb_tree* tree){
  _rb_destroy(tree->destroy,tree->root);
  free(tree);
}
