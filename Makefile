OBJDIR=obj
RBTREE=$(OBJDIR)/rbtree.o
CFLAGS+=-Wall -Wextra -Werror -s
.PHONY: all
.PRECIOUS: $(RBTREE)

all: $(addprefix $(OBJDIR)/,monitor test)
$(OBJDIR)/monitor: monitor.c $(RBTREE) | $(OBJDIR); $(CC) $(CFLAGS) -o $@ $^ -lpthread
$(OBJDIR)/%: %.c $(RBTREE) | $(OBJDIR); $(CC) $(CFLAGS) -o $@ $^
$(OBJDIR)/%.o: %.c | $(OBJDIR); $(CC) $(CFLAGS) -c -o $@ $^
$(OBJDIR):; mkdir -p $(OBJDIR)
