#!/usr/bin/perl

use strict;

sub permute {
  my ($remain,@args)=@_;
  my $count=@$remain;
  if(!$count){return system('obj/test',@args);}
  for (my $i=0;$i<$count;$i++){
    my @remain=@$remain;
    permute(\@remain,@args,splice(@remain,$i,1));
  }
}

permute(\@ARGV);
