#include <stdio.h>
#include <string.h>
#include "rbtree.h"

struct int_node {
  struct rb_node _;
  int value;
};

static int node_compare(const struct rb_node* A,const struct rb_node* B){
  return ((struct int_node*)A)->value-((struct int_node*)B)->value;
}
static size_t rb_check(struct rb_node* node,size_t* bhp){
  if(!node){return 0;}
  const int value=((struct int_node*)node)->value;
  if(node->red){
    if(!node->P){fprintf(stderr,"[%d] self(!P) is red\n",value);}
    if(node->L && node->L->red){
      fprintf(stderr,"[%d] self(red)->L is red\n",value);
    }
    if(node->R && node->R->red){
      fprintf(stderr,"[%d] self(red)->R is red\n",value);
    }
  }
  if(!node->L && !node->R){
    size_t bh=0;
    struct rb_node* x=node;
    while(x){
      if(!x->red){bh++;}
      x=x->P;
    }
    if(*bhp==0){*bhp=bh;}
    else if(*bhp!=bh){
      fprintf(stderr,"[%d] self::bh %zu != %zu\n",value,*bhp,bh);
    }
    return 1;
  }
  if(node->L && node->L->P!=node){
    fprintf(stderr,"[%d] self->L->P != self\n",value);
  }
  if(node->R && node->R->P!=node){
    fprintf(stderr,"[%d] self->R->P != self\n",value);
  }
  return rb_check(node->L,bhp)+1+rb_check(node->R,bhp);
}
static void rb_print(struct rb_node* node,int tab){
  if(!node){return;}
  rb_print(node->R,tab+1);
  for (int i=0;i<tab;i++){fputs("    ",stdout);}
  printf("%s%d%s\n",node->red?"\033[41m":"",
   ((struct int_node*)node)->value,node->red?"\033[m":"");
  rb_print(node->L,tab+1);
}

int main(int argc,char** argv) {
  struct rb_tree* tree=rb_new(node_compare,NULL);
  int insert=1;
  for (int i=1;i<argc;i++){
    struct int_node K;
    if(strcmp(argv[i],"--")==0){insert=!insert;}
    else if(strcmp(argv[i],"-?")==0){rb_print(tree->root,0);}
    else if(sscanf(argv[i],"%d",&K.value)==1){
      struct int_node* x=(void*)rb_get(tree,(void*)&K,insert?sizeof(*x):0);
      if(insert){x->value=K.value;}
      else if(x){rb_delete(tree,(void*)x);}
      size_t bh=0,count=rb_check(tree->root,&bh);
      printf("bh=%zu count=%zu\n",bh,count);
    }
    else {fprintf(stderr,"W: Invalid argument [%s]\n",argv[i]);}
  }
  rb_destroy(tree);
}
